
-- HELPERS
local graphics = love.graphics
local insert   = table.insert

-- CONSTS
local FONTPATH = "game/assets/fonts/"
local FONTLIST = require(FONTPATH)

-- MODULE
local Fonts = require 'common.class' :new()

function Fonts:instance (obj)
  local _sizes = {
    14, 20, 32, 48
  }
  local _families = {}

  for nickname, filename in pairs(FONTLIST) do
    _families[nickname] = {}
    for idx, fontsize in ipairs(_sizes) do
      local path = FONTPATH .. filename
      local font = graphics.newFont(path, fontsize)
      font:setLineHeight(1.5)
      _families[nickname][idx] = font
    end
  end

  function obj.setFont(name, size)
    graphics.setFont(_families[name][size])
    return graphics.getFont()
  end
end

return Fonts()
