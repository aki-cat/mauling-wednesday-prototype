
local PATHS = {
  activity = "game/activities/",
  gamestate = "game/gamestates/",
  view = "game/views/",
}

local template_name, module_name, container_name = ...
local path
local file
local filestring
local out
local sub

-- read from template
print(("templates/%s.lua"):format(template_name))
file = io.open(("templates/%s.lua"):format(template_name), "r")
filestring = file:read("*a")
file:close()

container_name = container_name or ""
path = PATHS[template_name] .. container_name
sub = module_name:gsub("^%l", string.upper)
out = filestring:gsub("sample", sub)
print(("%s/%s.lua"):format(path, module_name))
file = io.open(("%s/%s.lua"):format(path, module_name), "w")
file:write(out)
file:close()
