
-- DEPENDENCIES
local SCREEN    = require 'screen'
local CO        = require 'util.coroutine'
local Drawable  = require 'common.drawable'
local Decorator = require 'common.decorator'

-- functions shortcuts
local fps      = love.timer.getFPS
local graphics = love.graphics
local yield    = CO.yield

function displayFPS ()
  local w, h  = love.window.getMode()
  local text = graphics.newText(graphics.newFont(12), "FPS: ".. tostring(fps()))

  SCREEN.addDrawable(
    Decorator (
      Drawable (text, { w - 64, 8 }),
      function () graphics.setColor(255, 255, 255, 255) end,
      99 -- zindex
    ),
    4 -- layerid
  )

  while true do
    text:set("FPS: " .. tostring(fps()))
    yield()
  end
end

return function ()
  CO.play(displayFPS)
  return displayFPS
end
