
-- DEPENDENCIES
local SCREEN = require 'screen'
local Queue  = require 'lux.common.Queue'

local PROFILE = require 'game.profile'


-- MODULE
local Controls = {}

-- PRIVATE INFO
local _redirect = {
  __index = function (self, k)
    if k == "RELEASE_QUIT" then love.event.quit() end
    if k == "PRESS_PAUSE" then
      PROFILE.print()
      __FAST_FORWARD__ = true
    end
    if k == "RELEASE_PAUSE" then
      __FAST_FORWARD__ = false
    end
    return function () end
  end
}
local _mappedControls
local _inputQueue = Queue(32)

-- PUBLIC METHODS
function Controls.send (action)
  _inputQueue.push(action)
end

function Controls.flush ()
  _inputQueue.popAll()
end

function Controls.setMap (map)
  map = map or {}
  setmetatable(map, _redirect)
  _mappedControls = map
end

function Controls.update ()
  while not _inputQueue.isEmpty() do
    local act = _inputQueue.pop()
    _mappedControls[act]()
  end
end

Controls.setMap()
return Controls
