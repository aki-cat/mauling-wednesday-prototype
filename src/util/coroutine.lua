
-- DEPENDENCIES
local resume = coroutine.resume
local create = coroutine.create
local status = coroutine.status
local yield  = coroutine.yield
local unpack = table.unpack or unpack
local time   = function () return love.timer.getTime() * 1000 end

-- CONTST
local __NOTHING__ = function () end

-- PRIVATE STUFF
local _routines = {}
local _waiters = {}

-- PUBLIC METHODS
local play = function (f, ...)
  local rt = _routines[f]
  if not rt or status(rt[1]) == "dead" and rt[2] then
    _routines[f] = { create(f), true, { ... } }
    rt = _routines[f]
  end
  return resume(rt[1], ...)
end

local createWaiter = function (ms, handle, ...)
  local params = { ... }
  local start = time()
  local lam =  function ()
    local now = time()
    --print(("Waiter: %d/%d"):format(now-start, ms))
    if now - start >= ms then
      (handle or __NOTHING__)(unpack(params))
      return true
    end
    return false
  end
  table.insert(_waiters, lam)
end

local freeze = function (f)
  _routines[f][2] = false
end

local unfreeze = function (f)
  _routines[f][2] = true
end

local doAndDelay = function (f, delay)
  local last = 0
  return function()
    if time() - last > delay then
      f()
      last = time()
    end
  end
end

local delete = function (f)
  _routines[f] = nil
end

local update = function (f)
  for f, rt in pairs(_routines) do
    if rt[2] then play(f, unpack(rt[3])) end
  end
  local idx_list = {}
  for i, waiter in ipairs(_waiters) do
    if waiter() then table.insert(idx_list, i) end
  end
  for i = 1, #idx_list do
    local idx = idx_list[i]
    table.remove(_waiters, idx)
    idx_list[i] = nil
  end
end

return {
  play = play,
  freeze = freeze,
  unfreeze = unfreeze,
  delete = delete,
  update = update,
  create = create,
  resume = resume,
  status = status,
  yield = yield,
  time = time,
  doAndDelay = doAndDelay,
  createWaiter = createWaiter,
}

