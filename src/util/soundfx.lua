
-- DEPENDENCIES
local LIST = require 'common.list'

-- HELPERS
local Audio = love.audio
local insert = table.insert

-- CONSTS

-- MODULE
local SoundFX = require 'common.class' :new()

function SoundFX:instance (obj, filepath, buffer)
  local _sources = {}
  local _buffer = buffer or 4
  local _current = 1
  local _path = filepath

  for i = 1, _buffer do
    insert(_sources, Audio.newSource(_path, "static"))
  end

  function obj.nextBuffer ()
    _current = LIST.next(_current, _buffer)
  end

  function obj.play (mult)
    local src = _sources[_current]
    mult = mult or 1
    src:setPitch(mult)
    if src:isPlaying() then
      -- obj.nextBuffer()
      -- src = _sources[_current]
      src:stop()
      return src:play()
    else
      return src:play()
    end
  end
end

return SoundFX
