
-- MODULE
local GameState = require 'common.gamestate'
local sample = GameState:new()

function sample:instance (obj)
  GameState:inherit(obj)

  function obj.onLoad (...)
  end
  function obj.onFocus ()
  end
  function obj.onRest ()
  end
  function obj.onQuit ()
  end
  function obj.onUpdate (dt)
  end

end

return sample()
