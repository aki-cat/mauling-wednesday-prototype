
-- DEPENDENCIES
local SIGNALS  = require 'util.signal'
local CONTROLS = require 'util.controls'
local Activity = require 'common.activity'

-- MODULE
local sample = Activity:new()

function sample:instance (obj, ...)
  Activity:inherit(obj)
  local _param

  function obj.onStart(...)
    obj.stop()
  end

  function obj.onUpdate(dt)
    --
  end

  function obj.onStop()
    --
  end

end

return sample
