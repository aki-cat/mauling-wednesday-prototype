
require 'libs'
__VERBOSE__ = false

-- PARAMS
local _width, _height
local _load_scenario

-- MODULES
local SCREEN       = require 'screen'
local INPUT        = require 'util.input'
local STATEMANAGER = require 'util.statemanager'
local SIGNALS      = require 'util.signal'
local CO           = require 'util.coroutine'
local FPS          = require 'util.fps'
local PROFILE      = require 'game.profile'
local FLAGS        = require 'game.profile.flags'
local COLORS       = require 'common.colors'
local TIMER        = require 'hump.timer'

function log (...)
  if __VERBOSE__ then
    print(...)
  end
end

function love.load (...)
  local runflags = ...
  local debugflags = {}
  -- setup input
  love.mouse.setVisible(false)
  love.keypressed = INPUT.keyPressed
  love.keyreleased = INPUT.keyReleased
  love.math.setRandomSeed(tonumber(tostring(os.time()):sub(-7):reverse()))

  -- setup window
  _width, _height = 1024, 640
  love.window.setMode(_width, _height, {fullscreen=true})
  love.graphics.setBackgroundColor(COLORS.background)

  -- set save/load directory
  PROFILE.init()

  -- debug run flags
  for _,runflag in ipairs(runflags) do
    if runflag == "--fps" then FPS = FPS()
    elseif runflag == "--clean" then PROFILE.clear() PROFILE.loadGame()
    elseif runflag == "-v" then __VERBOSE__ = true
    elseif runflag:match("^%-%-scenario=") then
      _load_scenario = runflag:match("=(.+)")
    elseif runflag:match("%-f=") then
      table.insert(debugflags, runflag:match("=(.+)"))
    end
  end

  -- setup flags and save/load
  PROFILE.loadGame()
  print("loading...")
  PROFILE.print()

  for _, flagname in ipairs(debugflags) do
    FLAGS.temp[flagname] = true
  end

  -- get scenario from flags?
  _load_scenario = _load_scenario or FLAGS.place

  -- setup gamestate
  STATEMANAGER.load("Scenario", _load_scenario)
end

function love.update (dt)
  INPUT.update()
  CO.update()
  TIMER.update(dt)
  STATEMANAGER.update(dt)
  SIGNALS.resolveQueue()
  SCREEN.update()
end

function love.draw ()
  SCREEN.render()
end

function love.quit ()
  print()
  print()
  print("SAVED DATA:")
  PROFILE.print()
  print()
  print()
end
