
-- DEPENDENCIES
local CONTEXT = require 'common.menu'
local PROFILE = require 'game.profile'
local FLAGS   = require 'game.profile.flags'
local Stack   = require 'common.stack'

local DEBUGGER = {}

-- PRIVATES
local _display = false
local _PERCENT = 0
local _animation
local _depth = 1
local _flaglist
local _scroll
local _context_list = {
  [1] = "FLAG_TYPE",
  [2] = "CHOOSE_FLAG",
}
local lastmap
local map = {
  PRESS_UP = CONTEXT.previous,
  PRESS_DOWN = CONTEXT.next,
  PRESS_B = CONTEXT.cancel,
  PRESS_A = CONTEXT.confirm,
}

-- HELPERS
local w, h = love.window.getMode()

-- TWEENER
local function tween(from, to, pace)
  return function (dt)
    dt = dt or 1
    local step = (to - from)/pace
    if step*step > 0.01 then
      from = from + step * dt
    else
      from = to
    end
    return from
  end
end

-- INTERACTIVE CONTEXT
local function debugWindow()
  print("debug window")
  drawBackground()
  love.graphics.push()
  -- DRAW CONTAINER
  local context = _context_list[_depth]
  if CONTEXT.menuBegin(context, 32-w*(1-_PERCENT) , 32, _scroll) then

    if context == "FLAG_TYPE" then

      -- choose flag type
      local function chooselist(list)
        _flaglist = list
        _scroll = 8
        _depth = _depth + 1
      end
      if CONTEXT.menuItem("Temporary Flags") then
        chooselist(FLAGS.temp)
      end
      if CONTEXT.menuItem("Permanent Flags") then
        chooselist(FLAGS.perm)
      end

    elseif context == "CHOOSE_FLAG" then

      -- choose flag
      for flagname, value in pairs(_flaglist) do
        if CONTEXT.menuItem(flagname) then
          _flaglist[flagname] = not value
        end
      end

    end

  else
    depth = depth - 1
    if depth <= 0 then
      DEBUGGER.trigger()
      depth = 1
    end
  end
  CONTEXT.menuEnd()
  love.graphics.pop()
end

function DEBUGGER.trigger ()

  -- trigger menu
  _scroll = 2
  _depth = 1
  _display = not _display
  local debugger_state = _display and "OPEN " or "CLOSE "
  print(debugger_state.."DEBUGGER")
  if _display then _animation = tween(0, 1, 2)
  else _animation = tween(1, 0, 2) end
end


local function drawBackground ()
  love.graphics.setColor(255, 255, 255, _PERCENT * 128)
  love.graphics.rectangle("fill", 0, 0, w, h)
end

-- setup
local draw = love.draw
if love.draw == draw then
  love.draw = function (...)
    print("new draw")
    draw(...)
    _PERCENT = _animation()
    if _display then debugWindow() end
  end
end

return DEBUGGER.trigger
