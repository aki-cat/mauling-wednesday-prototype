
-- HELPERS
local abs = math.abs
local delta = love.timer.getDelta
local fps = love.timer.getFPS

-- CONSTS
local EPSILON = 0.01

-- MODULE
local TWEEN = {}

local CURVES = {}

function CURVES.linear (from, to, pace, start)
  return (to - start) / pace
end

function CURVES.ease_out (from, to, pace, start)
  return (to - from) / pace
end

function CURVES.ease_in (from, to, pace, start)
  return (from - start) / pace
end

function CURVES.ease_in_out (from, to, pace, start)
  return (to - from) / pace
end

-- PUBLIC METHODS
function TWEEN.start (from, to, pace, curve_type)
  local start = from
  return function ()
    local step = CURVES[curve_type](from, to, pace, start)
    if step*step > EPSILON and abs(to - start) > abs(from - start) then
      from = from + step
    else
      from = to
    end
    return from
  end
end

return TWEEN
