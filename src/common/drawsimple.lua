
local Drawable = require 'common.drawable'

-- HEPERS
local graphics = love.graphics
local insert   = table.insert

-- MODULE
local DrawSimple = Drawable:new()

function DrawSimple:instance (obj, zindex)
  Drawable:inherit(obj, "DRAWSIMPLE", {}, zindex)
  local _objects = {}

  -- PUBLIC METHODS
  function obj.getObject () end
  function obj.setObject () end

  function obj.push (drawobj)
    insert(_objects, drawobj)
  end

  function obj.clear ()
    for k in ipairs(_objects) do _objects[k] = nil end
  end

  function obj.draw ()
    graphics.push()
    for _, drawobj in ipairs(_objects) do
      local method = drawobj[1]
      graphics[method](unpack(drawobj, 2))
    end
    obj.clear()
    graphics.pop()
  end

end

return DrawSimple
