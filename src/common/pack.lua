
return function (dirpath)
  return setmetatable({}, {
    __index = function (self, module)
      return require(dirpath .. "." .. module)
    end
  })
end
