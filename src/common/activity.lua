
local function __nothing__() end

local Activity = require 'common.class' :new()

function Activity:instance (obj, priority)
  local _priority = priority or 1
  local _playing = false
  local _id

  function obj.setId (id)
    _id = id
  end

  function obj.getId ()
    return _id
  end

  function obj.getPriority ()
    return _priority
  end

  function obj.isPlaying ()
    return _playing
  end

  function obj.start (...)
    _playing = true
    obj.onStart(...)
  end
  function obj.stop ()
    obj.onStop()
    _playing = false
  end
  function obj.update (dt)
    if _playing then obj.onUpdate(dt) end
  end

  obj.onStart  = __nothing__
  obj.onStop   = __nothing__
  obj.onUpdate = __nothing__
end

return Activity
