
local function previousIndex (i, max)
  -- for use in circular indexes
  return (i + max - 2) % max + 1
end

local function nextIndex (i, max)
  -- for use in circular indexes
  return i % max + 1
end

local function remove (t, k)
  -- note: do not use in organised or hash lists
  -- if you do, i don't guarantee anything
  local N = #t if N == 0 then return end
  local obj = t[k]
  t[k] = t[N]
  t[N] = nil
  return obj
end

local function nextKey (t, k)
  return next(t, k) ~= nil and next(t, k) or next(t)
end

local function previousKey (t, k)
  -- for use in hash circular lists
  -- when you want to get that previous thing you had
  -- will not work as expected if the list is updated

  -- existence condition
  k = k or nextKey(t, k)
  if k == nil then return end

  local lastkey = nextKey(t, k)
  while nextKey(t, lastkey) ~= k do
    lastkey = nextKey(t, lastkey)
  end
  return lastkey
end

return {
  remove = remove,
  previous = previousIndex,
  next = nextIndex,
  previousKey = previousKey,
  nextKey = nextKey,
}
