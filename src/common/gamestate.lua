
-- DEPENDENCIES
local LIST = require 'common.list'

-- HELPERS
local function __nothing__() end

-- CLASS
local GameState = require 'common.class' :new()

function GameState:instance(obj, ...)
  local _activities = {}
  local _dirty = false

  local function cleanActivities ()
    table.sort(_activities, function (a, b)
      return a.getPriority() > b.getPriority()
    end)
    for id, activity in ipairs(_activities) do
      activity.setId(id)
    end
    _dirty = false
  end

  function obj.addActivity (activity)
    table.insert(_activities, activity)
    _dirty = true
  end

  function obj.removeActivity (activity)
    --[[ You should not try to remove an activity
    after adding it until the next frame.
    `getId` returns nil before `cleanActivities` is
    run at least once after activity has been added.
    You can always not start the activity
    until you're sure you'll use it. ]]
    local id = activity.getId()
    LIST.remove(_activities, id)
    _dirty = true
  end

  function obj.updateActivities (dt)
    if _dirty then cleanActivities() end
    for id, activity in ipairs(_activities) do
      activity.update(dt)
    end
  end

  function obj.load (...)
    obj.onLoad(...)
  end
  function obj.focus ()
    obj.onFocus()
  end
  function obj.update (dt)
    obj.updateActivities(dt)
    obj.onUpdate(dt)
  end
  function obj.rest ()
    obj.onRest()
  end
  function obj.quit ()
    obj.onQuit()
  end

  obj.onLoad   = __nothing__
  obj.onFocus  = __nothing__
  obj.onUpdate = __nothing__
  obj.onRest   = __nothing__
  obj.onQuit   = __nothing__
end

return GameState
