
local function makeMetaMethods (obj, mt)
  obj.__operators = obj.__operators or {}
  for k, f in pairs(obj.__operators) do
    mt["__" .. k] = f
  end
  return obj
end

local function initClass (self, super)
  if not super then return end
  initClass(self, super.__super)
  if super.__init and type(super.__init) == 'function' then
    super.__init(self)
  end
end

local function makeInherit (super, self)
  self.__index = self
  self.__super = super
  setmetatable(self, super)
  initClass(super, self)
  return self
end

local function makeInstance (self, obj, ...)
  local mt = {}
  local current_mt = getmetatable(obj)
  if current_mt then setmetatable(current_mt, mt)
  else setmetatable(obj, mt) end
  self:instance(obj, ...)
  return makeMetaMethods(obj, mt)
end

local Class = {}

function Class:instance (obj, ...)
end

function Class:inherit (obj, ...)
  return makeInstance(self, obj, ...)
end

function Class:super ()
  return getmetatable(self)
end

function Class:__call (...)
  local obj = { __operators = {} }
  makeInstance(self, obj, ...)
  return obj
end

function Class:__index (k)
  return getmetatable(self)[k]
end

function Class:__init ()
  self.__call = Class.__call
end

function Class:new ()
  return makeInherit(self, {})
end

return Class
