
local Stack = require 'common.class' :new()

function Stack:instance (obj)
  local _stack = {}
  local _size = 0

  function obj.push (e)
    _size = _size + 1
    _stack[_size] = e
  end

  function obj.pop ()
    local obj = _stack[_size]
    assert(obj, "Stack is empty, cannot be popped.")
    _size = _size - 1
    return obj
  end

  function obj.getHead ()
    return _stack[_size]
  end

  function obj.getSize ()
    return _size
  end

  function obj.flush ()
    for i = 1, _size do
      _stack[i] = nil
    end
    _size = 0
  end
end

return Stack
