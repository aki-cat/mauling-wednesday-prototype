
local SCREEN = require 'screen'
local FONTS = require 'fonts'
local COLORS = require 'common.colors'
local CO    = require 'util.coroutine'
local TWEEN = require 'common.tween'
local Queue = require 'lux.common.Queue'

local ContextMenu = {}

-- HELPERS
local insert = table.insert
local unpack = table.unpack or unpack

-- CONSTS
local P  = 16
local LH = 30

-- PRIVATE
local _menulist = {}
local _pos = { 0, 0 }
local _current = false
local _actions = {
  confirm = false,
  cancel = false,
  previous = false,
  next = false,
}
local _ycursor
local _count
local _width
local _height
local _scrolltop
local _tween
local _font = FONTS.setFont("roboto_regular", 2)
local _view = require 'common.drawsimple' ()
local _itemqueue = Queue(64)


SCREEN.addDrawable(_view, 3)

local function selection (val)
  _menulist[_current] = val or _menulist[_current]
  return _menulist[_current]
end

local function setPos (x, y)
  _pos[1] = x
  _pos[2] = y
  _width = 0
  _height = P
end

local function getCursorHeight()
  return _pos[2] + P*1.25 +
        (_scrollrange
        and (P+LH)*((selection() - _scrolltop))
        or (P+LH)*(selection()-1))
end

local function startCursorTween ()
  _tween = TWEEN.start(_ycursor or getCursorHeight(), getCursorHeight(), 2, "ease_out")
end

local function getAction()
  if _actions.cancel then
    for k in pairs(_actions) do _actions[k] = false end
    _ycursor = false
    return false
  end
  if _actions.previous then
    selection(selection() - 1)
  end
  if _actions.next then
    selection(selection() + 1)
  end
  return true
end

local function setItemDisplacement ()
  if _scrollrange then
    local line_height = LH + P
    while selection() - _scrolltop > _scrollrange do
      _scrolltop = _scrolltop + 1
    end
    while _scrolltop > selection() do
      _scrolltop = _scrolltop - 1
    end
  end
end

local function setItemText (item_name, disabled)
  _itemqueue.push { "setColor", disabled and COLORS.disabled or COLORS.text }
  _itemqueue.push { "print", item_name, _pos[1] + P, _pos[2] + _height }
end

local function indicateScrolling ()
  if _scrollrange then
    _view.push { "setColor", COLORS.text }
    if _scrolltop > 1 then
      _view.push { "polygon", "fill",
        {
          _width - _pos[1] - 0.0*P, _pos[2] + 2.0*P,
          _width - _pos[1] - 1.0*P, _pos[2] + 2.0*P,
          _width - _pos[1] - 0.5*P, _pos[2] + 1.5*P,
        }
      }
    end
    if _scrolltop + _scrollrange < _count then
      _view.push { "polygon", "fill",
        {
          _width - _pos[1] - 0.0*P, _height - 2.0*P + _pos[2],
          _width - _pos[1] - 1.0*P, _height - 2.0*P + _pos[2],
          _width - _pos[1] - 0.5*P, _height - 1.5*P + _pos[2],
        }
      }
    end
  end
end

local _co = CO.create(function()
  while true do
    _ycursor = _tween()
    CO.yield()
  end
end)

local function displayItemSelection()
  local x = _pos[1] - P/4
  local w, h = _width - P, LH + P/2
  startCursorTween()
  assert(CO.resume(_co))
  _view.push { "setColor", COLORS.text }
  _view.push { "polygon", "fill", {
    x,       _ycursor,
    x,       _ycursor + P,
    x + P/2, _ycursor + P/2 } }
end

function ContextMenu.begin(context_name, x, y, scrollrange)
  if context_name ~= _current then
    _count = 0
    _menulist[context_name] = 1
    _scrolltop = 1
    _current = context_name
    _ycursor = false
    _scrollrange = scrollrange or false
    for k in pairs(_actions) do _actions[k] = false end
  end
  setPos(x, y)
  return getAction()
end

function ContextMenu.item (item_name, disabled)
  _count = _count + 1
  _width = math.max(_font:getWidth(item_name) + 2*P, _width + 2*P)
  if not _scrollrange then
    setItemText(item_name, disabled)
    _height = _height+LH+P
    -- _height = _height+LH+P
  elseif _count >= _scrolltop and _count <= _scrolltop + _scrollrange then
    setItemText(item_name, disabled)
    -- _height = _scrollrange*LH + 2*P
    _height = _height+LH+P
  end
  if _actions.confirm and _count == selection() then
    return true
  end
end

function ContextMenu.finish ()
  -- reset actions and selection
  if selection() > _count then
    selection(1)
  elseif selection() <= 0 then
    selection(_count)
  end

  -- draw
  _view.push { "setFont", _font}
  _view.push { "setColor", COLORS.gui }
  _view.push { "rectangle", "fill", _pos[1], _pos[2], _width, _height }
  setItemDisplacement()
  indicateScrolling()

  if not _ycursor then _ycursor = getCursorHeight() end
  displayItemSelection()

  while not _itemqueue.isEmpty() do
    _view.push(_itemqueue.pop())
    _view.push(_itemqueue.pop())
  end

  _count = 0

  for k in pairs(_actions) do _actions[k] = false end
end

function ContextMenu.confirm ()
  _actions.confirm = true
end

function ContextMenu.cancel ()
  _actions.cancel = true
end

function ContextMenu.previous ()
  _actions.previous = true
end

function ContextMenu.next ()
  _actions.next = true
end

return ContextMenu
