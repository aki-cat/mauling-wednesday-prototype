
-- DEPENDENCIES
local STATEMANAGER = require 'util.statemanager'
local SIGNALS      = require 'util.signal'
local CONTROLS     = require 'util.controls'
local COMMANDS     = require 'lux.pack' 'game.commands'


-- HELPERS
local yield  = coroutine.yield
local unpack = table.unpack or unpack


-- CLASS
local GameState = require 'common.gamestate'
local State = GameState:new()

function State:instance (obj)
  GameState:inherit(obj)

  -- PRIVATE ATTRIBUTES
  local _scene
  local _scene_play
  local _end_scene
  local _command

  -- PRIVATE METHODS
  local scenePlayer
  local executeCommand
  local on_sceneDone

  function executeCommand (line)
    -- interpret current line
    local directive = line[1]
    local content = { unpack(line, 2) }

    -- print command arguments
    print("CMD: "..directive, table.concat(content, ", "))

    -- execute command
    COMMANDS[directive](unpack(content))
  end

  function scenePlayer ()
    for i = 1, #_scene do
      if not _end_scene then
        executeCommand(_scene[i])
      end
    end
    -- end_scene
    CONTROLS.setMap()
    return "DONE"
  end

  function on_sceneDone ()
    _end_scene = true
  end


  -- PUBLIC METHODS
  function obj.onLoad (parsed_script)
    _end_scene = false
    _scene = parsed_script
    _line = 1
    _scene_play = coroutine.create(scenePlayer)
    SIGNALS.addListener("sceneDone", on_sceneDone)
    print("SCENE START")
  end

  function obj.onUpdate (dt)
    if _end_scene then
      -- exit scene
      STATEMANAGER.pop()
    else
      -- play scene
      if select(2, assert(coroutine.resume(_scene_play))) == "DONE" then
        STATEMANAGER.pop()
      end
    end
  end

  function obj.onQuit ()
    print("SCENE END")
    SIGNALS.removeListener("sceneDone", on_sceneDone)
    CONTROLS.setMap()
  end

end

-- RETURN
return State()

