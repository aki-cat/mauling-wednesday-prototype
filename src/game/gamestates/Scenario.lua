
-- DEPENDENCIES
local STATEMANAGER = require 'util.statemanager'
local SIGNALS      = require 'util.signal'
local CONTROLS     = require 'util.controls'
local PARSER       = require 'util.parser'
local CO           = require 'util.coroutine'
local LIST         = require 'common.list'
local FLAGS        = require 'game.profile.flags'
local PROFILE      = require 'game.profile'

local Menu         = require 'game.activities.menu'


-- CONSTS
local ACTIONS = { "interact", "move", "wait" }


-- MODULE
local GameState = require 'common.gamestate'
local Scenario = GameState:new()

function Scenario:instance (obj)
  GameState:inherit(obj)
  local _scenario_name
  local _scenario
  local _neighbours
  local _menu

  local function isSceneAvailable (scene_name)
    assert(_scenario[scene_name],
      ("Scene does not exist: %s"):format(scene_name))
    local flags = _scenario[scene_name].flags
    local repeatable = _scenario[scene_name].repeatable
    -- if it has been played before, and it's not repeatable...
    if FLAGS.hasPlayed(_scenario_name, scene_name) and not repeatable then
      return false
    end
    -- check for trigger flag conditions
    for _, flag_check in ipairs(flags) do
      print("checking >>", flag_check, FLAGS.check(flag_check))
      if not FLAGS.check(flag_check) then
        return false
      end
    end
    return true
  end

  local function listAvailableScenes ()
    local available_scenes = {}
    print()
    print("AVAILABLE SCENES IN:", _scenario_name)
    for scene_name in pairs(_scenario) do
      if isSceneAvailable(scene_name) then
        table.insert(available_scenes, scene_name)
        print("+ "..scene_name)
      else
        print("- "..scene_name)
      end
    end
    if #available_scenes == 0 then
      print(">> could not find any scenes at the moment")
    end
    return available_scenes
  end


  local function playScene (scene_name)
    FLAGS.setPlayed(_scenario_name, scene_name)
    return STATEMANAGER.push("SceneManager", _scenario_name, scene_name)
  end

  local function autoPlayScene (auto_name)
    -- explicit autoplay call (command)
    print("CHECKING FOR AUTO PLAY...")
    if auto_name then
      return SIGNALS.sendNextFrame("runScenarioScene", auto_name)
    end
    -- implicit autoplay call (flag/move)
    for scene_name in pairs(_scenario) do
      if scene_name:match("[Aa]uto") then
        if isSceneAvailable(scene_name) then
          print(">> found:", scene_name)
          return SIGNALS.sendNextFrame("runScenarioScene", scene_name)
        end
      end
    end
    print(">> could not find any scenes to play on auto")
  end

  local function updateAvailableScenes ()
    _menu.setAvailable(listAvailableScenes(), _neighbours)
  end

  function obj.onLoad (scenario_name, auto_name)
    print("LOADING SCENARIO:", scenario_name)
    print("REQUESTED SCENE:", auto_name or "NONE")
    FLAGS.place = scenario_name
    PROFILE.saveGame()
    _scenario_name = scenario_name
    _scenario, _neighbours = PARSER(scenario_name)
    _menu = Menu()
    obj.addActivity(_menu)
    autoPlayScene(auto_name)
    obj.focus()
  end

  function obj.onQuit ()
    obj.rest()
    obj.removeActivity(_menu)
    _scenario = false
    _neighbours = false
    _menu = false
  end

  function obj.onFocus ()
    print()
    _menu.start(listAvailableScenes(), _neighbours)
    SIGNALS.addListener("runScenarioScene", playScene)
    SIGNALS.addListener("checkAutoPlay", autoPlayScene)
    SIGNALS.addListener("updateAvailableScenes", updateAvailableScenes)
    autoPlayScene()
  end

  function obj.onRest ()
    print()
    _menu.stop()
    SIGNALS.removeListener("runScenarioScene", playScene)
    SIGNALS.removeListener("checkAutoPlay", autoPlayScene)
    SIGNALS.removeListener("updateAvailableScenes", updateAvailableScenes)
  end

  function obj.onUpdate (dt)
  end

end

return Scenario()
