
-- DEPENDENCIES
local SOUND        = require 'sound'
local SIGNALS      = require 'util.signal'
local CO           = require 'util.coroutine'
local CONTROLS     = require 'util.controls'
local STATEMANAGER = require 'util.statemanager'

local LIST         = require 'common.list'


-- MODULE
local GameState = require 'common.gamestate'
local State = GameState:new()

function State:instance (obj)
  GameState:inherit(obj)
  -- PRIVATE STUFF
  local _scenes
  local _selected
  local _delay = 100


  -- ACTION MAPPING
  local function select_confirm ()
    print("CHOOSEN SCENE:", _selected)
    SOUND.playSFX("yes")
    STATEMANAGER.push("SceneManager", _scenes, _selected)
  end

  local function select_cancel ()
    print("RETURN TO SCNARIO LIST")
    SOUND.playSFX("no")
    STATEMANAGER.load("PickScenario")
  end

  local function cursorPrev ()
    SOUND.playSFX("cursor")
    _selected = LIST.previousKey(_scenes, _selected)
  end

  local function cursorNext ()
    SOUND.playSFX("cursor")
    _selected = LIST.nextKey(_scenes, _selected)
  end

  -- PUBLIC METHODS
  function obj.onLoad (scenelist)
    -- load values
    _delay = 100
    _scenes = scenelist
    _selected = next(_scenes)
    -- load action mapping
    obj.focus()
  end

  function obj.onFocus ()
    local select_prev = CO.doAndDelay(cursorPrev, _delay)
    local select_next = CO.doAndDelay(cursorNext, _delay)
    CONTROLS.setMap {
      PRESS_A = select_confirm,
      PRESS_B = select_cancel,
      PRESS_UP = select_prev,
      PRESS_LEFT = select_prev,
      PRESS_DOWN = select_next,
      PRESS_RIGHT = select_next
    }
  end

  function obj.onRest ()
    CONTROLS.setMap()
  end

  function obj.onUpdate (dt)
  end

  function obj.onQuit ()
    obj.rest()
  end


  -- GETTERS
  function obj.getSelected ()
    return _selected
  end

  function obj.getScenes ()
    return _scenes
  end
end

-- RETURN
return State()
