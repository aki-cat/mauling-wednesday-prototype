
-- CONSTS
local START = 11 * 60

-- MODULE
local function getRaw (readable_time)
  local h, m = readable_time:match("(%d+):(%d+)")
  h = tonumber(h)
  m = tonumber(m)
  return h * 60 + m - START
end

local function getReadable (raw_time)
  local actual = raw_time + START
  local hours = math.floor(actual / 60)
  local min = actual % 60
  return ("%02d:%02d"):format(hours, min)
end

local function getStart ()
  return START
end

return {
  getStart = getStart,
  getRaw = getRaw,
  getReadable = getReadable,
}
