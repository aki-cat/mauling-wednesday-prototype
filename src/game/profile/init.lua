
-- DEPENDENCIES
local FLAGS = require 'game.profile.flags'
local JSON  = require 'dkjson'

-- HELPERS
local filesystem = love.filesystem

-- CONSTS
local IDENTITY = "MaulingWednesday"
local FILENAME = "savefile"

-- PUBLIC METHODS
local function saveGame ()
  local strdata = JSON.encode({
    perm = FLAGS.perm,
    temp = FLAGS.temp,
    place = FLAGS.place,
  }, { indent = true })
  local data = filesystem.newFileData(strdata, FILENAME)
  local file = filesystem.newFile(FILENAME, "w")
  file:write(data)
  return file:close()
end

local function loadGame ()
  local filedata, err = filesystem.newFileData(FILENAME)
  if not filedata then saveGame() return end
  -- read and load
  local loaded_flags = JSON.decode(filedata:getString())
  FLAGS.perm = loaded_flags.perm
  FLAGS.temp = loaded_flags.temp
  FLAGS.place = loaded_flags.place
end

local function clear ()
  filesystem.remove(FILENAME)
end

local function init ()
  filesystem.setIdentity(IDENTITY)
end

local function printdata ()
  local strdata = JSON.encode ({
    perm = FLAGS.perm,
    temp = FLAGS.temp,
    place = FLAGS.place,
  }, { indent = true })
  print(strdata)
end

return {
  init = init,
  clear = clear,
  print = printdata,
  saveGame = saveGame,
  loadGame = loadGame,
}
