
local TIME = require 'game.profile.time'

local FLAGS = {}

FLAGS.perm = {}
FLAGS.temp = { time = 0 }
FLAGS.place = "Prologue: The Marmot"

local function checkExpr (expr, scope)
  local checker
  -- check if expression is valid within scope
  checker = assert(loadstring("return " .. expr))
  setfenv(checker, scope)
  return checker()
end

local function checkFlag (expr)
  local check = checkExpr(expr, {
    perm = FLAGS.perm,
    temp = FLAGS.temp
  })
  return check
end

local function checkTime (interval)
  local vals = {}
  local current = FLAGS.temp.time
  for stamp in interval:gmatch("[^%-]+") do
    table.insert(vals, TIME.getRaw(stamp))
  end
  return vals[1] <= current and current <= vals[2]
end

function FLAGS.check (f)
  local is_time = f:match("time%-(.+)")
  if is_time then return checkTime(is_time) end
  return checkFlag(f)
end

function FLAGS.deleteTemp ()
  FLAGS.temp = { time = 0 }
end

function FLAGS.deletePerm ()
  FLAGS.perm = {}
end

local function getSceneFlag (scenario, scene)
  return ("%s_%s_has_played"):format(scenario, scene)
    :gsub("%s", "_"):gsub("[?.%(%)\':,]", ""):lower()
end

function FLAGS.hasPlayed (scenario, scene)
  local has_played = getSceneFlag(scenario, scene)
  print(">>> checking...", has_played, FLAGS.check(has_played))
  return FLAGS.check("temp."..has_played)
end

function FLAGS.setPlayed (scenario, scene)
  local has_played = getSceneFlag(scenario, scene)
  FLAGS.temp[has_played] = true
end

function FLAGS.deleteAll ()
  deleteTemp()
  deletePerm()
end

return FLAGS
