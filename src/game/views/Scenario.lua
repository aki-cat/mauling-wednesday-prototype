
-- DEPENDENCIES
local SCREEN     = require 'screen'
local FONTS      = require 'fonts'
local FLAGS      = require 'game.profile.flags'
local TIME       = require 'game.profile.time'
local COLORS     = require 'common.colors'
local DrawSimple = require 'common.drawsimple'

-- MODULE
local GameState = require 'common.gamestate'
local Scenario = GameState:new()

function Scenario:instance (obj)
  GameState:inherit(obj)

  local _scenario_name
  local _view
  local _font
  local _height

  function obj.onLoad (scenario_name)
    _scenario_name = scenario_name
    _view = DrawSimple()
    _font = FONTS.setFont("roboto_regular", 2)
    _height = _font:getHeight()
    obj.focus()
  end

  function obj.onFocus ()
    SCREEN.addDrawable(_view, 2)
  end

  function obj.onRest ()
    SCREEN.removeDrawable(_view, 2)
  end

  function obj.onQuit ()
    obj.rest()
  end

  function obj.onUpdate (dt)
    if FLAGS.check("perm.prologue_done") then
      _view.push { "setFont", _font}
      _view.push { "setColor", COLORS.highlight }
      _view.push { "print", _scenario_name, 32, _height * 2 }
      _view.push { "print", TIME.getReadable(FLAGS.temp.time), 32, _height * 3 }
    end
  end

end

return Scenario()
