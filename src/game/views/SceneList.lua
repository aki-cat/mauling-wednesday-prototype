
-- DEPENDENCIES
local SCREEN       = require 'screen'
local FONTS        = require 'fonts'
local STATES       = require 'lux.pack' 'game.gamestates'
local Drawable     = require 'common.drawable'
local Decorator    = require 'common.decorator'
local GameState    = require 'common.gamestate'


-- HELPERS
local graphics = love.graphics
local abs      = math.abs
local min      = math.min
local sqrt     = math.sqrt


-- CONSTS
local LINE_HEIGHT = 24
local MARGIN = 48


-- MODULES
local GameState = require 'common.gamestate'
local View = GameState:new()

function View:instance (obj)
  GameState:inherit(obj)

  -- PRIVATE STUFF
  local _state = STATES.SceneList
  local _selected
  local _scenes
  local _hashorder
  local _drawable_titles
  local _title_decorators
  local _arrow

  local function selectDecorator (k)
    local idx = _hashorder[_selected]
    local dist = idx - k
    local maxdist = #_drawable_titles
    if abs(dist) <= 2 then
      graphics.translate(0, -LINE_HEIGHT * dist)
      graphics.setColor(0xff, 0xff, 0xff, (1 - abs(dist/3)) * 0xff)
    else
      graphics.setColor(0, 0, 0, 0)
    end
  end

  local function loadDrawables ()
    -- base values
    local w, h = love.window.getMode()
    local font = FONTS.setFont("roboto_regular", 2)
    local i = 1

    -- init elements
    for cname in pairs(_scenes) do
      local decor_obj, draw_obj
      _hashorder[cname] = i
      _drawable_titles[i] = graphics.newText(font, cname)

      draw_obj = Drawable(_drawable_titles[i], { MARGIN, h / 2 })
      decor_obj = Decorator(draw_obj, function () end)

      _title_decorators[i] = decor_obj
      i = i + 1
    end

    -- set decorators
    for i, decor_obj in ipairs(_title_decorators) do
      decor_obj.setDecor(function ()
        local k = i
        selectDecorator(k)
      end)
    end

    -- set arrow
    local arrow_height = LINE_HEIGHT / 2
    _arrow = Decorator(
        Drawable(graphics.newMesh({
          { 0, 0, 0, 0,
            0xff, 0xff, 0xff, 0xff },
          { 0, arrow_height, 0, 0,
            0xff, 0xff, 0xff, 0xff },
          { arrow_height * sqrt(3) / 2, arrow_height / 2, 0, 0,
            0xff, 0xff, 0xff, 0xff },
        }, "fan", "static"), {
          MARGIN - 24, h / 2 + (LINE_HEIGHT - arrow_height) / 3
        }
      ), function () graphics.setColor(0xff, 0xff, 0xff, 0xff) end
    )
  end

  -- PUBLIC METHODS
  function obj.onLoad ()
    _selected = _state.getSelected()
    _scenes   = _state.getScenes()
    _hashorder = {}
    _drawable_titles = {}
    _title_decorators = {}
    -- init drawables
    loadDrawables()
    obj.focus()
  end

  function obj.onQuit ()
    obj.rest()
  end

  function obj.onFocus ()
    for i, decor_obj in pairs(_title_decorators) do
      SCREEN.addDrawable(decor_obj, 2)
    end
    return SCREEN.addDrawable(_arrow, 2)
  end

  function obj.onRest ()
    for i, decor_obj in pairs(_title_decorators) do
      SCREEN.removeDrawable(decor_obj, 2)
    end
    return SCREEN.removeDrawable(_arrow, 2)
  end

  function obj.onUpdate (dt)
    _selected = _state.getSelected()
  end
end

return View()
