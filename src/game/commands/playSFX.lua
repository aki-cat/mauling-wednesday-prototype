
local SOUND = require 'sound'

return function (sfx_name, pitch)
  SOUND.playSFX(sfx_name, pitch)
end

