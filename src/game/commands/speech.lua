
-- DEPENDENCIES
local SOUND      = require 'sound'
local FONTS      = require 'fonts'
local SCREEN     = require 'screen'
local SIGNALS    = require 'util.signal'
local CONTROLS   = require 'util.controls'
local CO         = require 'util.coroutine'
local COLORS     = require 'common.colors'
local TextBox    = require 'common.textbox'
local DrawSimple = require 'common.drawsimple'

-- HELPERS
local yield = CO.yield
local time  = CO.time


-- CONSTS
local MG = 32
local PD = 24
local W, H = love.graphics.getDimensions()
local ALPHA_STEP = 0xff/8

local DROPTIME = 200
local FAST = 3
local SLOW = 1
local DELAY_BASE = 100/6
local DELAY = setmetatable({
  ["."]       = 4,
  [","]       = 2,
  ["\'"]      = 2,
  ["\""]      = 0,
  ["["]       = 0,
  ["]"]       = 0, }, {
    __index = function () return 1 end
  }
)
local PITCH = setmetatable({
  Ty      = 1.25,
  Hasha   = 1.00,
  Derek   = 0.50,
  Rick    = 0.75,
  Chad    = 0.75,
  Mane    = 0.40, }, {
  __index = function () return 1.00 end
  }
)

local function parseText(str)
  local out = {}
  local text = ""
  local neutral = COLORS.text
  local highlight = false
  if str:match("%[") then neutral = COLORS.disabled end
  for i = 1, #str do
    local ch = str:sub(i, i)
    if ch == '_' or ch == '*' then
      table.insert(out, highlight and COLORS.highlight or neutral)
      table.insert(out, text)
      highlight = not highlight
      text = ""
    else
      text = text .. ch
    end
  end
  table.insert(out, highlight and COLORS.highlight or neutral)
  table.insert(out, text)
  return out
end


return function(speaker, expression, line)
  -- PRIVATE ATTRIBUTES
  local _mapping = {}
  local _charspeed = 1
  local _advance_dialogue = false
  local _alpha = 0
  local _is_not_narrator = speaker ~= "Narrator"
  local _box = TextBox(speaker, expression,
    _is_not_narrator and line or line:gsub("%[", ""):gsub("%]", ""))
  local _view = DrawSimple()

  -- ACTION MAPPING
  local function nextBox ()
    SOUND.playSFX("yes")
    _advance_dialogue = true
  end

  local function accelerate ()
    _charspeed = FAST
  end

  local function skip ()
    if _box then _box.skip() end
  end

  local function advanceChar(n)
    for i = 1, n do _box:advance() end
    SOUND.playSFX("voice1", PITCH[speaker])
  end

  local function updateView()
    local r, g, b, w, h
    local LH
    local font = FONTS.setFont("roboto_regular", 2)
    LH = font:getHeight() * font:getLineHeight()

    _view.push {"push"}
    _view.push {"translate", MG, H - MG - 2*PD - 3*LH}

    local speaker_text = _box.getSpeaker()
    if _is_not_narrator then
      -- container box
      r, g, b = unpack(COLORS.gui)
      _view.push {"setColor", r, g, b, _alpha}
      _view.push {"rectangle", "fill", 0, 0, W - 2*MG, 2*PD + 3*LH}

      -- speaker
      _view.push {"push"}
      _view.push {"translate", 0, -LH-PD}
      w = font:getWidth(speaker_text)
      h = font:getHeight()
      r, g, b = unpack(COLORS.gui)
      _view.push {"setColor", r, g, b, _alpha}
      _view.push {"rectangle", "fill", 0, 0, w+PD, h+PD}
      r, g, b = unpack(COLORS.highlight)
      _view.push {"setColor", r, g, b, _alpha}
      _view.push {"print", speaker_text, PD/2, PD/2}
      _view.push {"pop"}

      w = W - 3*LH - MG*2 - PD*5
    else
      _view.push {"translate", W/2 - MG - 6*LH, - MG - 2*PD - 3*LH}
      w = W / 2 + 6*LH
    end

    _view.push {"push"}
    -- expression
    --r, g, b = unpack(COLORS.disabled)
    --_view.push {"setColor", r, g, b, _alpha}
    --_view.push {"rectangle", "fill", PD, PD, 3*LH, 3*LH}
    --_view.translate(2*PD + 3*LH)

    -- text
    local x, y = PD, PD
    r, g, b = unpack(COLORS.neutral)
    _view.push {"setColor", r, g, b, _alpha}
    _view.push {"printf", parseText(_box.getText()), x, y, w, "left"}

    -- pops
    _view.push {"pop"}
    _view.push {"pop"}
  end

  local function metayield()
    _charspeed = SLOW
    if not _advance_dialogue then
      _alpha = _alpha < 0xff and _alpha + ALPHA_STEP or 0xff
    else
      _alpha = _alpha > 0 and _alpha - ALPHA_STEP or 0
    end
    updateView()
    yield()
  end

  SCREEN.addDrawable(_view, 3)

  do
    -- add controls
    _mapping.HOLD_A = accelerate
    _mapping.PRESS_B = skip
    CONTROLS.setMap(_mapping)

    -- textbox animation
    while not _box:isDone() do

      -- advance char
      local waiting_for_char = true
      local char = _box:getChar()
      CO.createWaiter(DELAY_BASE * DELAY[char], function()
        advanceChar(_charspeed)
        waiting_for_char = false
      end)
      if __FAST_FORWARD__ then _alpha = 0xff; skip() break end
      while waiting_for_char do metayield() end

      metayield()
    end

    -- delay after box is done
    _mapping.HOLD_A = nil
    _mapping.PRESS_B = nil
    local waiting_for_delay = true
    CO.createWaiter(DROPTIME, function ()
      waiting_for_delay = false
    end)
    while waiting_for_delay do
      if __FAST_FORWARD__ then break end
      metayield()
    end

    -- wait for user input to end text box
    _mapping.PRESS_A = nextBox
    _mapping.PRESS_B = nextBox
    while not _advance_dialogue do
      if __FAST_FORWARD__ then _advance_dialogue = true end
      metayield()
    end
    _mapping.PRESS_A = nil
    _mapping.PRESS_B = nil
    while _alpha > 0 do
      if __FAST_FORWARD__ then _alpha = 0 end
      metayield()
    end
    CONTROLS.setMap()
  end

  -- remove controls
  SCREEN.removeDrawable(_view, 3)
end
