
-- DEPENDENCIES
local SOUND       = require 'sound'
local SIGNALS     = require 'util.signal'
local CONTROLS    = require 'util.controls'
local CONTEXTMENU = require 'common.menu'

-- HELPERS
local yield = coroutine.yield

-- CONSTS
local W, H = love.window.getMode()
local U, M, P = 32, 16, 16
local X, Y = 16, H - 3*U - 2*P - 2*M

return function (...)
  -- PRIVATE ATTRIBUTES
  local _options = {}
  local _consequences = {}
  local _ypos
  local content = { ... }

  -- set choice options
  for i, item in ipairs(content) do
    local line = item:match("[%s\t]*([^(%(.+%))]+)[%s\t]*")
    local scene_name = item:match("%([%s\t]*(.+)[%s\t]*%)")
    _options[i] = line
    _consequences[line] = scene_name
  end

  -- set box vertical position
  _ypos = Y - 2*M - 2*P - #_options*U

  -- set controls
  CONTROLS.setMap {
    PRESS_A = function ()
      SOUND.playSFX("yes")
      CONTEXTMENU.confirm()
    end,
    PRESS_UP = function ()
      SOUND.playSFX("cursor")
      CONTEXTMENU.previous()
    end,
    PRESS_DOWN = function ()
      SOUND.playSFX("cursor")
      CONTEXTMENU.next()
    end,
  }

  -- now choose!
  while true do
    if CONTEXTMENU.begin("choice", X, _ypos) then
      for i, line in ipairs(_options) do
        if CONTEXTMENU.item(line) then
          local scene_name = _consequences[line]
          SIGNALS.send("endDialogueBox")
          SIGNALS.send("playScene", scene_name)
          yield("DONE")
        end
      end
      CONTEXTMENU.finish()
    end
    yield()
  end
end

