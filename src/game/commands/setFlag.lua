
-- DEPENDENCIES
local SIGNALS  = require 'util.signal'
local FLAGS    = require 'game.profile.flags'

return function (flagname, value, is_perm)

  local function eval (value, env)
    local checker = loadstring("return " .. tostring(value))
    setfenv(checker, env)
    return checker
  end

  local new_value = eval(value, is_perm and FLAGS.perm or FLAGS.temp)()
  if is_perm then
    FLAGS.perm[flagname] = new_value
  else
    FLAGS.temp[flagname] = new_value
  end

  print(("%s = %s, %s"):format(flagname,
                               new_value,
                               is_perm and "perm" or "temp"))

end

