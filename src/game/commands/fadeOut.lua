
-- DEPENDENCIES
local SIGNALS  = require 'util.signal'
local OVERLAY  = require 'common.overlay'
local CO       = require 'util.coroutine'

return function (time)
  local ms = time * 1000
  local done = false
  CO.createWaiter(ms, function () done = true end)
  SIGNALS.send("endDialogueBox")
  OVERLAY.darken(time)
  while not done do CO.yield() end
end

