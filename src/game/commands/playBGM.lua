
local SOUND = require 'sound'

return function (bgm_name, effect)
  SOUND.playBGM(bgm_name, effect)
end

